drop table if exists tb_orders;

create table tb_orders(
  order_id serial not null primary key,
  order_date timestamp not null,
  order_status varchar(10) not null,
  user_id integer not null,
  product_id integer not null unique
);

drop table if exists tb_products;

create table tb_products(
  product_id serial not null primary key,
  product_title varchar(100) not null,
  product_author varchar(150) not null,
  product_description text not null,
  product_country varchar(70) not null,
  product_launch_date timestamp not null,
  product_is_available boolean not null,
  product_price numeric not null
);

drop table if exists tb_users;

create table tb_users(
  user_id serial not null primary key,
  user_name varchar(200) not null,
  user_birthdate timestamp not null,
  user_cpf varchar(11) not null unique,
  user_email varchar(150) not null unique,
  user_phone varchar(11) not null unique,
  user_role varchar(10) not null,
  user_password text not null
);
