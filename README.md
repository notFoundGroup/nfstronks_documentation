# Documentation

## Architecture
![nftstronks architecture](architecture.png)

Composed of:

- 3 APIs:
     - Auth-API: Used for all authentication. This API generates the user Token and is a security-gateway for all requests.
     - Users-API: Manages all users in the system.
     - Orders-API: Manages all orders in the system. This API uses Kafka-Spring to send records to the Kafka Cluster.
     - Products-API: Manages all products. This api uses AWS S3 for storing the products' images.
- 1 Microservice:
     - Payment gateway: In charge to confirm the async payments and to send the confirmation email for the user.
- 1 Kafka-Cluster:
     - With 2 brokers, was used Strimzy to up it.
- 1 Kubernetes Cluster:
     - AWS EKS.

## Before running the project:
- Setup a PostgreSQL database;
- Run the script `schema.sql` located in this repository;
- Configure the project's environment variables;

## 💻 Run the project:
 - Clone this project locally;
 - Update the docker-compose file;
 - Finally you can up the docker-compose and enjoy.
